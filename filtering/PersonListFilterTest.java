package filtering;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static filtering.PersonListFilter.Predicates.bornBefore;
import static filtering.PersonListFilter.Predicates.nameContains;
import static filtering.PersonListFilter.Predicates.streetAddressContains;
import static filtering.PersonListFilter.Predicates.postOfficeEquals;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class PersonListFilterTest {

	@Test
	void testNameContains() {
		List<Person> filtered = new PersonListFilter().filter(
				nameContains("Esko"),
				getTestPersons());
		
		assertTrue(filtered.size() == 3);
	}
	
	@Test
	void testNameAndBornBefore() {
		List<Person> filtered = new PersonListFilter().filter(
				bornBefore(LocalDate.of(1998, 1, 1)).and(nameContains("esko")),
				getTestPersons());
		
		assertTrue(filtered.size() == 2);
	}

	@Test
	void testStreetAddressContains() {
		List<Person> filtered = new PersonListFilter().filter(
				streetAddressContains("katu"), 
				getTestPersons());
		
		assertTrue(filtered.size() == 2);
	}

	@Test
	void testPostOffice() {
		List<Person> filtered = new PersonListFilter().filter(
				postOfficeEquals("Turku"), 
				getTestPersons());
		
		assertTrue(filtered.size() == 1);
	}

	private List<Person> getTestPersons() {
		return Arrays.asList(
				new Person(UUID.randomUUID(), 
						"Matti Esko",
						Arrays.asList(new Address("Viinikankatu", "33100", "Tampere")), 
						LocalDate.of(1985, 12, 12)),
				new Person(
						UUID.randomUUID(), 
						"Aki Esko",
						Arrays.asList(new Address("Havumetsänkatu", "33820", "Tampere")), 
						LocalDate.of(1999, 1, 12)),
				new Person(
						UUID.randomUUID(), 
						"Esko Petterson",
						Arrays.asList(new Address("Lenkkipolku", "20520", "Turku")), 
						LocalDate.of(1970, 1, 1)));
	}

}
