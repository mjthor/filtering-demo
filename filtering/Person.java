package filtering;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Person {
	private UUID id;
	private String name;
	private List<Address> addresses = new ArrayList<>();
	private LocalDate birthday;

	public Person(UUID id, String name, List<Address> addresses, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.addresses = addresses;
		this.birthday = birthday;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAddresses() {
		return new ArrayList<>(addresses);
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = new ArrayList<>(addresses);
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
}