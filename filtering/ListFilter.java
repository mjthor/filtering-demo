package filtering;

import java.util.List;
import java.util.function.Predicate;

public interface ListFilter<T> {

	List<T> filter(Predicate<T> filter, List<T> items);

}