package filtering;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PersonListFilter implements ListFilter<Person> {

	@Override
	public List<Person> filter(Predicate<Person> filter, List<Person> persons) {
		return persons.stream().filter(filter).collect(Collectors.toList());
	}

	static class Predicates {

		public static Predicate<Person> idEquals(final UUID id) {
			return p -> p.getId().equals(id);
		}

		public static Predicate<Person> nameContains(final String str) {
			return p -> p.getName().toLowerCase().contains(str.toLowerCase());
		}

		public static Predicate<Person> bornBefore(final LocalDate date) {
			return p -> p.getBirthday().isBefore(date);
		}

		public static Predicate<Person> bornAfter(final LocalDate date) {
			return p -> p.getBirthday().isAfter(date);
		}

		public static Predicate<Person> streetAddressContains(final String str) {
			return p -> !p.getAddresses().stream()
					// Extract Street addresses
					.map(a -> a.getStreetAddress())
					// Filter street addresses containing str, ignore case
					.filter(sa -> sa.toLowerCase().contains(str.toLowerCase()))
					// Collect filtered results into a List
					.collect(Collectors.toList())
					// Check that the resulting List is not empty
					.isEmpty();
		}

		public static Predicate<Person> postOfficeEquals(final String postoffice) {
			return p -> p.getAddresses().stream()
					// Extract post offices
					.map(a -> a.getPostoffice())
					// Collect post office Strings into a List
					.collect(Collectors.toList())
					// Check if the result List contains the given postoffice
					.contains(postoffice);
		}

		public static Predicate<Person> postNumberEquals(final String postNumber) {
			return p -> p.getAddresses().stream().map(a -> a.getPostnumber()).collect(Collectors.toList())
					.contains(postNumber);
		}

	}

}
