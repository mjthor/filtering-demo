package filtering;

public class Address {
	private String streetAddress;
	private String postnumber;
	private String postoffice;

	public Address(String streetAddress, String postnumber, String postoffice) {
		this.streetAddress = streetAddress;
		this.postnumber = postnumber;
		this.postoffice = postoffice;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostnumber() {
		return postnumber;
	}

	public void setPostnumber(String postnumber) {
		this.postnumber = postnumber;
	}

	public String getPostoffice() {
		return postoffice;
	}

	public void setPostoffice(String postoffice) {
		this.postoffice = postoffice;
	}
}